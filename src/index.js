const app = require('./config/server.js');
const routes = require("./app/routes/routes");

routes.about(app);
routes.authors(app);
routes.bares(app);
routes.contact(app);
routes.home(app);
routes.login(app);
routes.auth(app);
routes.logout(app);
routes.cadastrobares(app);
routes.formcadastrobares(app);
routes.cadastrobaressalvar(app);
routes.bar(app);
routes.contatodeusuarios(app);
routes.excluirbar(app);
routes.contatosalvar(app);