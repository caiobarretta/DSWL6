let express = require('express');
let expressSession = require('express-session');


let app = express();

let port = process.env.PORT || 3000;
app.set('views', './views');

app.use(express.static('./public'));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(expressSession({
    secret: 'Teste',
    resave: false,
    saveUninitialized: false
}));

app.listen(port, function(){
    console.log("Runnning at: ", port);
});

module.exports = app;