const {about} = require('../controllers/about')
const {authors} = require('../controllers/authors')
const {bares, getBar, deleteBar} = require('../controllers/bares')
const {contact, contatoSalvar, contatoRenderErros} = require('../controllers/contact')
const {home} = require('../controllers/home')
const {Login, Auth, Logout} = require('../controllers/auth')
const {check, validationResult } = require('express-validator')
const {CadastroBares, FormCadastroBares, CadastroBaresSalvar, FormCadastrobaresRenderErros} = require('../controllers/cadastrobares')
const {contatoDeUsuarios} = require('../controllers/contatoDeUsuarios')

//Rotas – Devem existir pelo menos 5 rotas)
module.exports = {
    about: (app) => {
        app.get('/about', (req, res) =>{
            about(app, req, res);
        });
    },

    authors: (app) => {
        app.get('/authors', (req, res) =>{
            authors(app, req, res);
        });
    },

    bares: (app) =>{
        app.get('/bares', (req, res) =>{
            bares(app, req, res);
        });
    },

    contact: (app) => {
        app.get('/contact', (req, res) =>{
            contact(app, req, res);
        });
    },

    contatosalvar : (app) =>{
        app.post("/contato-salvar", [
            check("nome")
                .notEmpty().withMessage('Nome não pode ser vazio'),
            check("fone")
                .notEmpty().withMessage('Telefone não pode ser vazio'),
            check("email")
                .notEmpty().withMessage('Email não pode ser vazio'),
            check("email")
                .isEmail().withMessage('Email inválido'),
            check("fone")
                .matches(/^\s*(\d{2}|\d{0})[-. ]?(\d{5}|\d{4})[-. ]?(\d{4})[-. ]?\s*$/).withMessage('Formato de telefone inválido.'),
        ], (req, res) => {
            //Validação dos dados
            const validation = validationResult(req);
            if(!validation.isEmpty()){
                contatoRenderErros(app, req, res, validation.array());
                return;
            }
            contatoSalvar(app, req, res);
        });
    },

    home: (app) =>{
        app.get('/', (req, res) =>{
            home(app, req, res);
        });
    },

    login: (app) =>{
        app.get('/login', (req, res) => {
            Login(app, req, res);
        });
    },

    auth: (app) => {
        //Validação dos dados
        app.post('/auth-login', [
            check("email")
                .notEmpty().withMessage('email não pode ser vazio'),
            check("password")
                .notEmpty().withMessage('senha é um campo obrigatório')
        ], (req, res) => {
            //Validação dos dados
            const validation = validationResult(req);
            if(!validation.isEmpty()){
                res.render('auth.ejs', {usuario: req.body, erros: validation.array()});
                return;
            }
            Auth(app, req, res);
        });
    },

    logout: (app) =>{
        app.get('/logout', (req, res) => {
            Logout(app, req, res);
        });
    },

    cadastrobares: (app) => {
        app.get('/cadastrobares', (req, res) => {
            CadastroBares(app, req, res);
        });
    },

    formcadastrobares: (app) => {
        app.get('/formcadastrobares', (req, res) => {
            FormCadastroBares(app, req, res);
        });
    },

    cadastrobaressalvar: (app) =>{
        //Validação dos dados
        app.post("/cadastrobares-salvar", [
            check("nome")
                .notEmpty().withMessage('Nome não pode ser vazio'),
            check("endereco")
                .notEmpty().withMessage('Endereço é um campo obrigatório'),
            check("lat")
                .notEmpty().withMessage('Latitude é um campo obrigatório'),
            check("lat")
                .isNumeric().withMessage('Formato Latitude inválido.'),
            check("long")
                .notEmpty().withMessage('Longitude é um campo obrigatório'),
            check("long")
                .isNumeric().withMessage('Formato Longitude inválido.'),
            check("urlimagem")
                .notEmpty().withMessage('Url é um campo obrigatório'),
           check("urlimagem")
                .matches(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/).withMessage('Formato URL inválido.'),                
        ], (req, res) => {
            //Validação dos dados
            const validation = validationResult(req);
            if(!validation.isEmpty()){
                FormCadastrobaresRenderErros(app, req, res, validation.array());
                return;
            }
            CadastroBaresSalvar(app, req, res);
        });
    },

    bar: (app) => {
        app.get('/bar', (req, res) => {
            getBar(app, req, res);
        });
    },

    excluirbar: (app) =>{
        app.get('/excluirbar', (req, res) =>{
            deleteBar(app, req, res);
        });
    },

    contatodeusuarios: (app) =>{
        app.get('/contatodeusuarios', (req, res) =>{
            contatoDeUsuarios(app, req, res);
        });
    }
}