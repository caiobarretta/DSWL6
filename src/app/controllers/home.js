const {getMenu} = require('../models/menuLinks');

module.exports.home = (app, req, res) => {
    res.render('home.ejs', {email: req.session.email, menu: getMenu(req)});
}