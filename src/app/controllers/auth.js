const dbConnection =  require('../../config/dbConnection')
const {authUsuario} = require('../models/usuarios');
const logger = require('../../config/logger');
const {getMenu} = require('../models/menuLinks');

module.exports.Login = (app, req, res) => {
    res.render('auth.ejs', {usuario: {}, erros: null, menu: getMenu(req)});
}

module.exports.Auth = (app, req, res) => {
    dbconn = dbConnection();
    const usuario = req.body;

    //usuario.password = crypto.createHash("md5").update(usuario.password).digest("hex");
    usuario.password = usuario.password;

    authUsuario(usuario, dbconn, (error, result) =>{
        if (error){
            logger.log({
                level: 'error',
                message: error.message
            });
            res.status(500).render('viewError.ejs', {error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
            return;
        }

        if(result.length <= 0){
            erros = [{msg: 'Senha ou usuário inválido.'}];
            res.render('auth.ejs', {usuario: {}, erros: erros});
            return;
        }
        req.session.autorizado = true;
        req.session.email = usuario.email;
        req.session.perfilid = result[0].perfilid;
        res.redirect('/');
    });
}

module.exports.Logout = (app, req, res) => {
    req.session.destroy();
    res.redirect('/');
}