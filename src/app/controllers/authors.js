const {getMenu} = require('../models/menuLinks');

module.exports.authors = (app, req, res) => {
    res.render('authors.ejs', {email: req.session.email, menu: getMenu(req) });
}