const dbConnection = require('../../config/dbConnection');
const logger = require('../../config/logger');
const { getMenu } = require('../models/menuLinks');
const { getMotivoContato, getMotivoContatos } = require('../models/motivocontatos');
const { getContato, insertContato } = require('../models/contatos');

module.exports.contact = (app, req, res) => {
    dbconn = dbConnection();
    if (req.query.contatosid) {
        getContato(req.query.contatosid, dbconn, (error, contato) => {
            if (error) {
                logger.log({
                    level: 'error',
                    message: error.message
                });
                //Tratamento de erros
                res.status(500).render('viewError.ejs', { menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde." });
                return;
            }
            dbconn = dbConnection();
            getMotivoContatos(dbconn, (error, result) => {
                if (error) {
                    logger.log({
                        level: 'error',
                        message: error.message
                    });
                    //Tratamento de erros
                    res.status(500).render('viewError.ejs', { menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde." });
                    return;
                }
                res.render('contact.ejs', { email: req.session.email, menu: getMenu(req), mensagem: null, contact: contato[0], motivocontato: result, isView: true, erros: {} });
                return;
            });
            return;
        });
    }
    else {
        getMotivoContatos(dbconn, (error, result) => {
            if (error) {
                logger.log({
                    level: 'error',
                    message: error.message
                });
                //Tratamento de erros
                res.status(500).render('viewError.ejs', { menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde." });
                return;
            }
            res.render('contact.ejs', { email: req.session.email, menu: getMenu(req), mensagem: null, contact: {}, motivocontato: result, isView: false, erros: {} });
            return;
        });
    }
}

module.exports.contatoSalvar = (app, req, res) => {
    dbconn = dbConnection();
    console.log(req.body);
    insertContato(req.body, dbconn, (error, resultInsert) => {
        if (error) {
            logger.log({
                level: 'error',
                message: error.message
            });
            //Tratamento de erros
            res.status(500).render('viewError.ejs', { email: req.session.email, menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde." });
            return;
        }
        dbconn = dbConnection();
        getMotivoContatos(dbconn, (error, result) => {
            if (error) {
                logger.log({
                    level: 'error',
                    message: error.message
                });
                //Tratamento de erros
                res.status(500).render('viewError.ejs', { email: req.session.email, menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde." });
                return;
            }
            res.render('contact.ejs', { email: req.session.email, menu: getMenu(req), mensagem: "Mensagem enviada com sucesso.", contact: req.body, motivocontato: result, isView: false, erros: {} });
            return;
        });
    });

}


module.exports.contatoRenderErros = (app, req, res, erros) => {
    dbconn = dbConnection();
    getMotivoContatos(dbconn, (error, result) => {
        if (error) {
            logger.log({
                level: 'error',
                message: error.message
            });
            //Tratamento de erros
            res.status(500).render('viewError.ejs', { email: req.session.email, menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde." });
            return;
        }
        res.render('contact.ejs', { email: req.session.email, menu: getMenu(req), bar: req.body, erros: erros, mensagem: null, isView: false, contact: req.body, motivocontato: result });
        return;
    });
    
}