const {getMenu} = require('../models/menuLinks');

module.exports.about = (app, req, res) => {
    res.render('about.ejs', {email: req.session.email, menu: getMenu(req)});
}