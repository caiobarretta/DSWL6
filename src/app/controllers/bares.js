const dbConnection =  require('../../config/dbConnection')
const {getBares, getBar, deleteBar} = require('../models/bares');
const logger = require('../../config/logger');
const {getMenu} = require('../models/menuLinks');

module.exports.bares = (app, req, res) => {

    dbconn = dbConnection();
    getBares(dbconn, (error, result) =>{
        //Tratamento de erros
        if (error){
            logger.log({
                level: 'error',
                message: error.message
            });
        }
        res.contentType('application/json');
        res.send(JSON.stringify(result));
    });
}


module.exports.getBar = (app, req, res) => {
    //Validação dos dados perfil administrador
    if(!req.session.perfilid || req.session.perfilid != 1){
        error = `Ops, aparentemente você não possui permissão ou sua sessão foi inspirada. 
                        Atualize a página ou entre em contato com o administrador`;
        res.status(500).render('viewError.ejs', {menu: getMenu(req), error});
        return;
    }
    
    dbconn = dbConnection();
    barid = req.query.barid;
    getBar(barid, dbconn, (error, result) =>{
        if (error){
            logger.log({
                level: 'error',
                message: error.message
            });
            //Tratamento de erros
            res.status(500).render('viewError.ejs', {menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
            return;
        }
        res.render('bar.ejs', {email: req.session.email, menu: getMenu(req), erros: {}, bar: result[0]});
    });
}

module.exports.deleteBar = (app, req, res) => {
    //Validação dos dados perfil administrador
    if(!req.session.perfilid || req.session.perfilid != 1){
        error = `Ops, aparentemente você não possui permissão ou sua sessão foi inspirada. 
                        Atualize a página ou entre em contato com o administrador`;
        res.status(500).render('viewError.ejs', {menu: getMenu(req), error});
        return;
    }
    
    dbconn = dbConnection();
    barid = req.query.barid;
    if(barid){
        deleteBar(barid, dbconn, (error, result) =>{
            if (error){
                logger.log({
                    level: 'error',
                    message: error.message
                });
                //Tratamento de erros
                res.status(500).render('viewError.ejs', {menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
                return;
            }
            res.redirect('/cadastrobares');
        });
        return;
    }
    //Tratamento de erros
    res.status(500).render('viewError.ejs', {menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
    return;
}