const {getMenu} = require('../models/menuLinks');
const {getContatos} = require('../models/contatos');
const dbConnection =  require('../../config/dbConnection');

module.exports.contatoDeUsuarios = (app, req, res) => {
    //Validação dos dados perfil administrador
    if(!req.session.perfilid || req.session.perfilid != 1){
        error = `Ops, aparentemente você não possui permissão ou sua sessão foi inspirada. 
                        Atualize a página ou entre em contato com o administrador`;
        res.status(500).render('viewError.ejs', { menu: getMenu(req), error});
        return;
    }

    dbconn = dbConnection();
    getContatos(dbconn, (error, result) =>{
        //Tratamento de erros
        if (error){
            logger.log({
                level: 'error',
                message: error.message
            });
            //Tratamento de erros
            res.status(500).render('viewError.ejs', { menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
            return;
        }
        
        res.render('contatodeusuarios.ejs', {email: req.session.email, menu: getMenu(req), contato: result});
    });
    
}