const logger = require('../../config/logger');
const dbConnection =  require('../../config/dbConnection')
const {getBares, getBar, updateBar, insertBar} = require('../models/bares');
const {getMenu} = require('../models/menuLinks');


module.exports.CadastroBares = (app, req, res) => {
    //Validação dos dados perfil administrador
    if(!req.session.perfilid || req.session.perfilid != 1){
        error = `Ops, aparentemente você não possui permissão ou sua sessão foi inspirada. 
                        Atualize a página ou entre em contato com o administrador`;
        res.status(500).render('viewError.ejs', {menu: getMenu(req), error});
        return;
    }

    dbconn = dbConnection();
    getBares(dbconn, (error, result) =>{
        //Tratamento de erros
        if (error){
            logger.log({
                level: 'error',
                message: error.message
            });
            //Tratamento de erros
            res.status(500).render('viewError.ejs', {menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
            return;
        }
        
        res.render('cadastrobares.ejs', {email: req.session.email, menu: getMenu(req), erros: {}, bares: result});
    });
}

module.exports.FormCadastroBares = (app, req, res) => {
    //Validação dos dados perfil administrador
    if(!req.session.perfilid || req.session.perfilid != 1){
        error = `Ops, aparentemente você não possui permissão ou sua sessão foi inspirada. 
                        Atualize a página ou entre em contato com o administrador`;
        res.status(500).render('viewError.ejs', {menu: getMenu(req), error});
        return;
    }
    
    barid = req.query.barid;
    action = req.query.action;
    if(barid && action == "Edit"){
        dbconn = dbConnection();
        getBar(barid, dbconn, (error, result) =>{
            
            if (error){
                logger.log({
                    level: 'error',
                    message: error.message
                });
                //Tratamento de erros
                res.status(500).render('viewError.ejs', {menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
                return;
            }
            res.render('formcadastrobares.ejs', {email: req.session.email, menu: getMenu(req), erros: {}, bar: result[0]});
        });
        return;
    }
    res.render('formcadastrobares.ejs', {email: req.session.email, menu: getMenu(req), erros: {}, bar: {}});
}

module.exports.CadastroBaresSalvar = (app, req, res) => {
    //Validação dos dados perfil administrador
    if(!req.session.perfilid || req.session.perfilid != 1){
        error = `Ops, aparentemente você não possui permissão ou sua sessão foi inspirada. 
                        Atualize a página ou entre em contato com o administrador`;
        res.status(500).render('viewError.ejs', {menu: getMenu(req), error});
        return;
    }

    bar = req.body;
    dbconn = dbConnection();

    if(bar.barid){
        updateBar(bar, dbconn, (error, result) =>{
            if (error){
                logger.log({
                    level: 'error',
                    message: error.message
                });
                //Tratamento de erros
                res.status(500).render('viewError.ejs', {menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
                return;
            }
            res.redirect(`/bar?barid=${bar.barid}`);
        });
        return;
    }
    insertBar(bar, dbconn, (error, result) =>{
        if (error){
            logger.log({
                level: 'error',
                message: error.message
            });
            //Tratamento de erros
            res.status(500).render('viewError.ejs', {menu: getMenu(req), error: "Ocorreu um erro, atualize a página ou tente novamente mais tarde."});
            return;
        }
        res.redirect('/cadastrobares');
    });
    return;
}

module.exports.FormCadastrobaresRenderErros = (app, req, res, erros) =>{
    res.render('formcadastrobares.ejs', {email: req.session.email, menu: getMenu(req), bar: req.body, erros: erros});
}