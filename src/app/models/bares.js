module.exports = {
    getBares: (dbConn, callback) =>{
        //Leitura dos dados
        const sql = 'select * from bares;';
        dbConn.query(sql, callback);
    },

    getBar: (barid, dbConn, callback) =>{
        //Leitura dos dados
        const sql = `select * from bares where barid = ${barid}`;
        dbConn.query(sql, callback);
    },

    updateBar: (bar, dbConn, callback) =>{
        const sql = `update bares set
        nome = "${bar.nome}", descricao = "${bar.descricao}", 
        endereco = "${bar.endereco}", insta = "${bar.insta}", 
        fb = "${bar.fb}", lat = "${bar.lat}",
        \`long\` = "${bar.long}", \`urlimagem\` = "${bar.urlimagem}"
        where barid = ${bar.barid}`;
        dbConn.query(sql, callback);
    },

    insertBar: (bar, dbConn, callback) =>{
        const sql = `insert into bares
            (\`nome\`, descricao, endereco, insta, fb, 
              lat, \`long\`, \`urlimagem\`)
        VALUES 
            ("${bar.nome}", "${bar.descricao}", "${bar.endereco}", "${bar.insta}", "${bar.fb}", 
             "${bar.lat}", "${bar.long}", "${bar.urlimagem}")`;
        dbConn.query(sql, callback);
    },

    deleteBar: (barid, dbConn, callback) =>{
        //Leitura dos dados
        const sql = `delete from bares where barid = ${barid}`;
        dbConn.query(sql, callback);
    },
}