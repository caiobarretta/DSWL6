module.exports = {
    getMenu: (req) =>{
       
        itensMenu = [
            //<li><a href="/">Home</a></li>
            {href: "/", texto: "Home"},
            //<li><a href="/about">Sobre o projeto</a></li>
            {href: "/about", texto: "Sobre o projeto"},
            //<li><a href="/authors">Autores</a></li>
            {href: "/authors", texto: "Autores"},
            //<li><a href="/contact">Contato</a></li>
            {href: "/contact", texto: "Contato"}
        ]
        //<% if(email) { %> <li><a href="/logout">logout</a></li> <% } %>
        if(req.session.email){
            itensMenu.push({href: "/logout", texto: "Logout"});
        }
        //<% if(!email) { %> <li><a href="/login">login</a></li> <% } %>
        else{
            itensMenu.push({href: "/login", texto: "Login"});
        }
        //<% if(perfilid == 1) { %> <li><a href="/cadastrobares">Cadastro Bares</a></li> <% } %>
        if(req.session.perfilid && req.session.perfilid == 1){
            itensMenu.push({href: "/cadastrobares", texto: "Cadastro Bares"});
            itensMenu.push({href: "/contatodeusuarios", texto: "Contatos de Usuários"});
        }
        return itensMenu;
    }
}