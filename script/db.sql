#--Criação do banco de dados
drop database IF EXISTS bares;
create database bares;

use bares;

#--Criação da tabela bares
DROP TABLE IF EXISTS bares;
CREATE TABLE `bares`.`bares` (
    `barid` INT NOT NULL AUTO_INCREMENT,
    `nome` VARCHAR(100) NOT NULL, 
    descricao VARCHAR(250),
    endereco VARCHAR(200) NOT NULL,
    insta VARCHAR(200),
    fb VARCHAR(200),
    lat VARCHAR(200) NOT NULL,
    `long` VARCHAR(200) NOT NULL,
    `urlimagem` VARCHAR(1000) NOT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `update_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`barid`)
);


#--Massa Inicial tabela bares
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Kitnet Bar",
	"Casa noturna da cena alternativa apresenta pocket shows vocais e acústicos, com pegada emo dark e temática.",
	"Av. Francisco José de Camargo Andrade, 145 - Jardim Chapadão, Campinas - SP, 13070-143",
	"https://www.instagram.com/kitnetbar",
	"https://www.facebook.com/kitnetbar",
	"-22.88770065271771",
	"-47.076896176717746",
	"https://pbs.twimg.com/profile_images/807322189762621440/HJZVmzQg_400x400.jpg"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Livre Club Bar",
	"Casa noturna com música eletrônica, discotecagem mix, pista de dança, bar, e predominância de público LGBT.",
	"Av. Francisco Glicério, 2165 - Vila Itapura, Campinas - SP, 13023-101",
	"https://www.instagram.com/livrebar",
	"https://www.facebook.com/livreclubar/",
	"-22.896352453664974",
	"-47.06544946137421",
	"https://storage.lorean.com.br/branded/20237/logo.png?nocache=043627"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Club 88",
	"Noites agitadas com drinques, DJs e os sons da música eletrônica, em espaço charmoso de edifício histórico.",
	"Av. Dr. Thomaz Alves, 39 - Centro, Campinas - SP, 13010-170",
	"https://www.instagram.com/club88oficial",
	"https://www.facebook.com/club88oficial/",
    "-22.902696602990318",
	"-47.059371716949975",
	"https://freight.cargo.site/t/original/i/37fd7e45699b25404e72673cc75d3afb00dea8498775fa93fcbba7e647c3bc92/88-roses.jpg"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Up Music Bar",
	"Bar com música ao vivo",
	"Av. Andrade Neves, 1890 - Castelo, Campinas - SP, 13070-000",
    NULL,
    "https://pt-br.facebook.com/UpMusicBar",
    "-22.89216860207143",
	"-47.07500848465645",
	"https://fastly.4sqi.net/img/general/600x600/56286592_3ndV45viotqWTHxqidZUDrizgIU8Q_Gn-uKbxm3JChA.jpg"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Gus Bar",
	"Boteco simples e descontraído com mesas em terraço coberto serve cervejas populares e porções tradicionais.",
	"Galeria Conceição Plaza, Sala 9, R. Conceição, 480 - Centro, Campinas - SP, 13050-010",
    "https://pt-br.facebook.com/GUSbarCampinas/",
	"https://www.instagram.com/gusclubroma/",
    "-22.90293729588766",
	"-47.0552479613742",
	"https://fastly.4sqi.net/img/general/200x200/40851982_gO829d5ERoT-AtU8CyhD0MEc1MxVvjiiiUmcDSmgUyY.jpg"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"SubWay Club",
	"Casa noturna com MPB ao vivo e pista de dança também tem um espaço lounge, jardins e shows de drag queens.",
	"R. Maria Monteiro, 30 - Cambuí, Campinas - SP, 13025-150",
	"https://www.facebook.com/ClubSubwayCampinas",
	"https://www.instagram.com/clubsubwayoficial/",
    "-22.90285862308045",
	"-47.049781761374206",
	"https://fastly.4sqi.net/img/general/200x200/45085834_eFRbApGpMhTkxkuyM3KpEDd0KeW11rO6JLf6jRiPIEw.jpg"
);

INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Red Lion Pub",
	"Bar de estilo pub com chope e cervejas nacionais e importadas, porções fartas e rock, em atmosfera intimista.",
	"R. Dr. Guilherme da Silva, 102 - Cambuí, Campinas - SP, 13025-070",
    NULL,
	"https://pt-br.facebook.com/pages/category/Dance---Night-Club/Red-Lion-Pub-Oficial-193518274077406/",
    "-22.896869790634355",
	"-47.057619200000005",
	"https://www.baressp.com.br/bares/fotos2/red_lion_pub_2-min.jpg"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Bar do André",
    "Bar",
	"R. Dr. Sales de Oliveira, 201 - Vila Industrial (Campinas), Campinas - SP, 13035-270",
	"https://www.instagram.com/botecodoandre/",
    "https://www.facebook.com/botecodoandreoficial/",
    "-22.911630380995827",
	"-47.06237323068711",
	"https://media-cdn.tripadvisor.com/media/photo-s/0b/39/ee/38/bar-do-andre.jpg"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Nosso Bar",
	"Boteco charmoso de banquetas no balcão e mais de 180 tipos de cerveja, servindo também pratos e sanduíches.",
	"Rua Barão de Jaguara, 988 - Box 2 - Centro, Campinas - SP, 13010-040",
    "https://www.instagram.com/nossobarmcz/",
	"https://pt-br.facebook.com/pages/category/Beer-Garden/Nosso-bar-355412934631642/",
    "-22.904668650267087",
	"-47.058151084656444",
	"https://64.media.tumblr.com/ac8ba96ef98a4df64c2b8b8b4c711220/tumblr_mo6m33HrUo1srg84to1_500.png"
);
INSERT INTO `bares`.`bares` (`nome`, descricao, endereco, insta, fb, lat, `long`, `urlimagem`) VALUES(
	"Moustache Single's Bar",
	"Bar",
	"R. Irmãos Bierrembach, 127 - Cambuí, Campinas - SP, 13024-150",
	"https://www.instagram.com/explore/locations/429605335/brazil/campinas-sao-paulo/moustache-singles-bar/",
    "https://m.facebook.com/MoustacheSinglesBar/?locale2=pt_BR",
    "-22.894924144508025",
	"-47.0574145",
	"http://moustache.com.br/wp-content/uploads/2017/02/logo-retina-500x387.png"
);


#--Criação da tabela perfil
DROP TABLE IF EXISTS `perfils`;
CREATE TABLE `bares`.`perfils`(
	`perfilid` INT NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(20) NOT NULL,
	`descricao` VARCHAR(100) NOT NULL,
	`created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `update_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`perfilid`)
);

INSERT INTO `bares`.`perfils` (`nome`, `descricao`) 
VALUES('Administrador', 'Administrador do sistema');

INSERT INTO `bares`.`perfils` (`nome`, `descricao`) 
VALUES('Usuario', 'Usuário comum do sistema');

#--Criação da tabela usuarios
DROP TABLE IF EXISTS `bares`.`usuarios`;
CREATE TABLE `bares`.`usuarios`(
	`userid` INT NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(100) NOT NULL,
	`password` VARCHAR(100) NOT NULL,
	`perfilid` INT NOT NULL,
	`created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `update_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`userid`),
	FOREIGN KEY (`perfilid`) REFERENCES `perfils`(`perfilid`)
);

INSERT INTO `bares`.`usuarios` (`email`, `password`, `perfilid`) VALUES('admin@bares.com.br', 'admin', 1);

#--Criação da tabela usuariosbaresfavoritos
DROP TABLE IF EXISTS `bares`.`usuariosbaresfavoritos`;
CREATE TABLE `bares`.`usuariosbaresfavoritos`(
	`usuariosbaresfavoritosid` INT NOT NULL AUTO_INCREMENT,
	`barid` INT NOT NULL,
    `userid` INT NOT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `update_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`usuariosbaresfavoritosid`),
	FOREIGN KEY (`barid`) REFERENCES `bares`(`barid`),
    FOREIGN KEY (`userid`) REFERENCES `usuarios`(`userid`)
);

INSERT INTO `bares`.`usuariosbaresfavoritos` (`barid`, `userid`) VALUES(1, 1);

#--Criação da tabela perfil
DROP TABLE IF EXISTS `motivocontatos`;
CREATE TABLE `bares`.`motivocontatos`(
	`motivocontatosid` INT NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(20) NOT NULL,
	`descricao` VARCHAR(100) NOT NULL,
	`created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `update_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`motivocontatosid`)
);

INSERT INTO `bares`.`motivocontatos` (`nome`, `descricao`) VALUES('Sobre o projeto', 'Sobre o projeto');
INSERT INTO `bares`.`motivocontatos` (`nome`, `descricao`) VALUES('Contribuições', 'Contribuições');
INSERT INTO `bares`.`motivocontatos` (`nome`, `descricao`) VALUES('Outros', 'Outros');

#--Criação da tabela contato
DROP TABLE IF EXISTS `contato`;
CREATE TABLE `bares`.`contatos`(
	`contatosid` INT NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(100) NOT NULL,
	`fone` VARCHAR(20) NOT NULL,
	`email` VARCHAR(100) NOT NULL,
	`comentariosgerais` VARCHAR(1000) NOT NULL,
    `lido` BOOLEAN DEFAULT FALSE,
	`motivocontatosid` INT NOT NULL,
    `userid` INT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `update_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`contatosid`),
    FOREIGN KEY (`userid`) REFERENCES `usuarios`(`userid`),
    FOREIGN KEY (`motivocontatosid`) REFERENCES `motivocontatos`(`motivocontatosid`)
);

INSERT INTO `bares`.`contatos` 
	(`nome`, `fone`, `email`, `motivocontatosid`, `userid`, `comentariosgerais`) 
VALUES 
	('Carla Elaine Renata Fogaça', '(95) 2669-3006', 'carla.elaine.fogaca@effem.com', 1, NULL,
	'Consectetur quisque ullamcorper vitae accumsan tincidunt fames quis facilisis, mauris vehicula maecenas sapien lacus nam eleifend posuere curabitur, sapien congue arcu tortor ad sagittis massa. a congue placerat vivamus pretium netus sociosqu lacus, eu ultricies nulla sodales curabitur mauris iaculis, ad cursus sem taciti quam curabitur. suscipit amet mauris malesuada vehicula lectus eleifend accumsan magna etiam laoreet, pellentesque rutrum nulla iaculis conubia mattis ad blandit aliquam, sociosqu nisl rhoncus ultrices nullam fames aenean quam semper. cubilia feugiat luctus platea nulla rhoncus dolor lacinia class, donec ultricies tortor morbi fames imperdiet. ');

INSERT INTO `bares`.`contatos` 
	(`nome`, `fone`, `email`, `motivocontatosid`, `userid`, `comentariosgerais`) 
VALUES 
	('Henrique Calebe Cláudio Baptista', '(71) 3800-5065', 'henrique_baptista@delfrateinfo.com.br', 2, NULL,
	'Eu faucibus felis torquent aenean posuere laoreet elit risus amet libero, diam sit adipiscing dictumst ante sit primis sagittis porttitor rhoncus, nullam nunc sem habitant velit posuere purus amet nulla. convallis aliquam tellus a justo dictumst neque, vestibulum habitant ultricies quis vel curabitur hendrerit, habitant etiam hendrerit quam pulvinar nam, at aliquam erat facilisis mauris. nulla metus pulvinar at bibendum viverra placerat ut aptent senectus fringilla, augue eu non aenean ante nullam dapibus torquent interdum, vulputate praesent habitasse urna euismod semper urna massa urna. arcu aliquam vulputate suspendisse curae quam auctor iaculis eros, convallis nisi augue class eleifend fames lobortis.');

INSERT INTO `bares`.`contatos` 
	(`nome`, `fone`, `email`, `motivocontatosid`, `userid`, `comentariosgerais`) 
VALUES 
	('Yago Thales Luan da Cruz', '(65) 2768-4725', 'yago-dacruz82@grupomegavale.com.br', 3, NULL,
	'Taciti eget sociosqu hac tellus pellentesque quam feugiat, per ut ultricies ut porta lacinia quisque, nibh ligula potenti aenean dolor aenean. ut nisl conubia aliquam cras venenatis lobortis etiam platea, et ac odio fringilla quisque consequat blandit, neque et eleifend vulputate non justo magna. habitant leo fringilla pretium integer quis nulla vehicula varius nostra nullam, gravida lobortis quisque faucibus hac torquent commodo velit tempor urna, scelerisque adipiscing ac taciti fringilla taciti nullam inceptos in. donec nulla sit molestie mattis adipiscing imperdiet potenti etiam pulvinar, sit platea aliquam donec blandit accumsan porta commodo.');
